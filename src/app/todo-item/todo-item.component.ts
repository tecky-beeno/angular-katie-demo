import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { TodoItem } from '../todo-item'
import { TodoListService } from '../todo-list.service'

@Component({
  selector: 'todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss'],
})
export class TodoItemComponent implements OnInit {
  @Input()
  item!: TodoItem

  constructor(private todoListService: TodoListService) {}

  ngOnInit(): void {}

  tick() {
    this.item.count++
  }

  deleteItem(){
    this.todoListService.deleteItem(this.item)
  }
}
