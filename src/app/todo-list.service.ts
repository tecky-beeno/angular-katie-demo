import { Injectable } from '@angular/core'
import { TodoItem } from './todo-item'

@Injectable({
  providedIn: 'root',
})
export class TodoListService {
  list: TodoItem[] = [
    { text: 'Buy milk', count: 0 },
    { text: 'Buy banana', count: 0 },
    { text: 'Buy apple', count: 0 },
    { text: 'Buy orange', count: 0 },
  ]

  constructor() {}

  deleteItem(item: TodoItem) {
    let index = this.list.indexOf(item)
    this.list.splice(index, 1)
  }
}
