import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  a = 1
  b = 2

  get c() {
    return this.a + this.b
  }

  ngOnInit() {
    setInterval(() => {
      this.b++
    }, 3000)
  }
}
