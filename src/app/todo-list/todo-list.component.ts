import { Component, OnInit } from '@angular/core'
import { TodoListService } from '../todo-list.service'

@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnInit {
  get list() {
    return this.todoListService.list
  }

  constructor(private todoListService: TodoListService) {}

  ngOnInit(): void {}
}
